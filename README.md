# Crossword Puzzle Creator and Solver


## Prerequisites
Java 1.8 or above

You need to clone the repository. Otherwise the default dictionnary will not be set. You can download individually data/frgut.txt and load it as a dictionnary via the file menu.
Any dictionnary can be loaded, as long as it is a file containing 1 world per line.

## Description
Software that helps you create or solve any grid of a crossword puzzle

### File Menu
In the file menu you can ...
* Create a new grid, specify the number of row / col (Take into consideration that the higher the number of free squarre in the grid, the longer it will take to solve the grid)
* Open a grid file (.grl). it stores a grid, can be modified with a text editor.
* Save a grid. I recommand naming your file "NameOfTheFile.grl" to be able to load it whenever you want to edit it again.
* Load a dictionnary. Basically a dictionnary is a file containing a word on each line. The default one is a french one so beware.

### Buttons
* Clear button, resets the grid as it was when you load it / created it first.
* Solve button, tries to solve the grid (10x10 with only open squarres is very time consuming for this software).
* Mode Create, this mod allows you to edit the grid. If you click on a "*" squarre, it will become available for a letter to be in the squarre. On the other hand, if you click on a squarre that can be occupied by a letter, it will set it to a squarre that cannot be occupied anymore.
* Mode Edit, this mod allows you to edit the grid, you can click on any squarre that can be occupied by a letter, and set the letter that you want in it.

## Structure
Grid files (.grl) are strutured like this. The first line holds the number of rows then a space and the number of columns

Then it writes down every character of the grid following the structure above.

- "*" Means that they cannot be a character in the squarre
- " " Means that they can be a character in the squarre and it is missing
- "X" Means that X occupy the squarre.

## Running

To start the application use
- Java -jar motcroise.jar

You can either use some grid available in the data folder or create your own grid. Push the solve button to get the grid solved

A good example is to run the data/larger.grl grid
## Authors
* Jérémy Nguyen

