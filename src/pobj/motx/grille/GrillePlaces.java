package pobj.motx.grille;

import java.util.*;

import pobj.motx.structure.Case;
import pobj.motx.structure.Emplacement;


public class GrillePlaces{
	private List<Emplacement> places = new ArrayList<Emplacement>();
	private int nbHorizontal = 0;
	private Grille grille;
	
	

	public GrillePlaces(Grille grille){
		this.grille = grille;
		for(int i = 0; i < grille.nbLig(); i++){
			List<Case> caseExaminer = getLig(i);
			cherchePlaces(caseExaminer);
		}
		//System.out.println(places.toString());
		nbHorizontal = places.size();
		for(int i = 0; i < grille.nbCol(); i++){
			List<Case> caseExaminer = getCol(i);
			cherchePlaces(caseExaminer);
		}
		
		
	}

	private List<Case> getLig(int lig){
		List<Case> list = new ArrayList<Case>();
		for(int j = 0; j < grille.nbCol(); j++){
			list.add(grille.getCase(lig,j));
		}
		return list;
	}

	private List<Case> getCol(int col){
		List<Case> list = new ArrayList<Case>();
		for(int i = 0; i < grille.nbLig(); i++){
			list.add(grille.getCase(i,col));
		}
		return list;
	}

	public void cherchePlaces(List<Case> cases){
		Emplacement e = new Emplacement();	
		if(cases.size() <= 1){
			return;
		}	
		for(Case c:cases){
			if(c.isPleine()){
				if(e.size() > 1){
					places.add(e);
				}
				e = new Emplacement();
			}	
			else{
				e.add(c);
			}
		}
		if(e.size() > 1){
			places.add(e);
		}
	}
	

	public GrillePlaces fixer(int m,String soluce) {
	
		
		//Nouvelle grille
		Grille g = grille.copy();
		
		GrillePlaces gp = new GrillePlaces(g);
		
		/*while(m < gp.getPlaces().size() && gp.getPlaces().get(m).estPlein() ) {
			m++;
		}
		*/
		// if m == gp.getPlaces().size()
		// Alors on throw pour dire qu'on a finit. et qu'il n'y a pas de solution
		
		//System.out.println(gp.getPlaces().size());
		
		// Traitement
		
		Emplacement mot = gp.getPlaces().get(m);
		//System.out.println("GrillePlaces FIXER 01 : "+m+ " // " + mot.getLettre().get(0).toString());
		//System.out.println(mot.toString());
		//System.out.println(gp.getPlaces().get(9).toString());
		//System.out.println("FLAG 001" + gp.getPlaces().get(m).toString());
		
		//Compteur de l'indice de la lettre
		int cpt=0;
		for(Case c:mot.getLettre()) {
			
			char laLettre = soluce.charAt(cpt);
			c.setChar(laLettre);
			//System.out.println(c.toString() + c.getChar());
			cpt++;
		}
		
		
		// v3
		//System.out.print("TEST : " + gp.getPlaces().remove(m));
		//System.out.println(gp.toString());
		//System.out.println(gp.getPlaces().get(0).toString()+"Nombre de places apres remmove");
		
		// Retour de la nouvelle grille
		return gp;
	
	}
	
	public List<Emplacement> getHorizontaux(){
		List<Emplacement> listHor = new ArrayList<Emplacement>();
		for(int i = 0;i<nbHorizontal;i++) {
			listHor.add(places.get(i));
		}
		return listHor;
	}
	
	public List<Emplacement> getVerticaux(){
		List<Emplacement> listVert = new ArrayList<Emplacement>();
		int limit = places.size();
		for(int i = nbHorizontal;i<limit;i++) {
			listVert.add(places.get(i));
		}
		return listVert;
	}
	

	public List<Emplacement> getPlaces(){
		return places;
	}

	public int getNbHorizontal(){
		return nbHorizontal;
	}

	public String toString(){
		return grille.toString();
	}
	
	public Grille getGrille() {
		return this.grille;
	}

}