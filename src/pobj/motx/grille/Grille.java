package pobj.motx.grille;

import pobj.motx.structure.Case;

public class Grille{

	private Case ca[][];
	private int lig;
	private int col;

	public Grille(int hauteur, int largeur){
		this.lig = hauteur;
		this.col = largeur;
		ca = new Case[hauteur][largeur];

		for(int i = 0; i < lig; i++){
			for(int j = 0; j < col; j++){
				ca[i][j] = new Case(i,j,' ');
			}
		}
	}


	// COPY
	public Grille copy(){
		Grille copy = new Grille(lig,col);
		for(int i = 0; i < lig; i++){
			for(int j = 0; j < col; j++){
				copy.getCase(i,j).setChar(ca[i][j].getChar());
			}
		}
		return copy;
	}

	// ACCESSEUR
	public Case getCase(int lig, int col){
		return ca[lig][col];
	}
	
	public int nbLig(){
		return lig;
	}
	public int nbCol(){
		return col;
	}

	//toString
	public String toString(){
		String output = "\n";
		for(int i = 0; i < lig; i++){
			for(int j = 0; j < col; j++){
				output = output + " " + ca[i][j].getChar() + " ";
			}
			output = output + "\n";
		}
		return output;
	}

}