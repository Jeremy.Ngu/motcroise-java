package pobj.motx.grille;

import java.util.ArrayList;
import java.util.List;

import pobj.motx.filtre.CroixContrainte;
import pobj.motx.interfaces.IContrainte;
import pobj.motx.structure.Case;
import pobj.motx.structure.Dictionnaire;
import pobj.motx.structure.Emplacement;

public class GrillePotentiel{
	private GrillePlaces gp;
	private Dictionnaire d;
	private List<Dictionnaire> motsPot = new ArrayList<Dictionnaire>();
	private List<IContrainte> contraintes = new ArrayList<IContrainte>();
	private List<Dictionnaire> listDico;
	
	
	public GrillePotentiel(GrillePlaces grille, Dictionnaire dicoComplet){
		this.gp = grille;
		this.d = dicoComplet;

		// Filtrer
		for(Emplacement i:this.gp.getPlaces()) {
			//if(!i.estPlein()) {
				Dictionnaire tempD = this.d.copy();
				tempD.filtreLongueur(i.size());
				
				
	
				
				int cpt = 0;
				
				
				for(Case c:i.getLettre()) {
					char chr = c.getChar();
					if(chr == ' ') {
						//System.out.println(cpt + "blank");
					}
					else {
						//System.out.print(cpt);
						//System.out.println(c.getChar());
						tempD.filtreParLettre(chr, cpt);
					}
					cpt ++;
				}
				//System.out.println(tempD.size());
				motsPot.add(tempD);
			//}
		}
		
		//System.out.println(grille.toString());
		
		// Detection contraintes
		
		for(Emplacement i:grille.getHorizontaux()) {
			for(Emplacement j:grille.getVerticaux()) {
				for(Case c1:i.getLettre()) {
					for(Case c2:j.getLettre()) {
						if(c1.equals(c2) && c1.isVide()) {
							int im1,im2,ic1,ic2;
							
							//System.out.println("c1 "+ c1.getCol()+ "_" + c1.getLig());
							//System.out.println("c2 "+c2.getCol() + "_"+ c2.getLig());
							

							
							im1 = grille.getPlaces().indexOf(i);
							im2 = grille.getPlaces().indexOf(j);
							
							ic1 = i.getLettre().indexOf(c1);
							ic2 = j.getLettre().indexOf(c2);
							
							
							//System.out.println(" " + im1 + " " + im2 + " " + ic1+ " " + ic2);
							IContrainte contrainteToAdd = new CroixContrainte(im1,ic1,im2,ic2);
							contraintes.add(contrainteToAdd); 
							//System.out.println(contraintes.size());
						}
					}
				}
			}
		}
		
		
		
		if(this.propage()) {
			System.out.println("Fini");
		}
		else {
			System.out.println("Irr�alisable");
		}

	}
	
	public boolean propage() {
		while(true) {
			int motsSupp = 0;
			for(IContrainte i : contraintes) {
				motsSupp += i.reduce(this);
			}
			
			if(isDead()) {
				return false;
			}
			
			if(motsSupp == 0) {
				return true;
			}
		}
	}
	
	
	public List<IContrainte> getContraintes(){
		return contraintes;
	}
	
	public GrillePlaces getgrillePlaces() {
		return gp;
	}
	
	public boolean isDead() {
		for (Dictionnaire d:motsPot) {
			if (d.size() == 0){
				return true;
			}
		}
		return false;
	}
	
	public GrillePotentiel fixer(int m,String soluce) {
		
//		System.out.println("01 " + m + soluce);
//		System.out.println("02 " + gp.getPlaces().get(m).toString());
//		System.out.println("03 " + getMotsPot().get(m).toString());
//		System.out.println("04 " + getMotsPot().size());
//		System.out.println("05 " + gp.getPlaces().size());
//		System.out.println("06 " + gp.getPlaces().get(m).getLettre().get(0).toString());

		
		GrillePlaces gpTemp = gp.fixer(m, soluce);
		
		//gpTemp.getPlaces().remove(m);
		
		GrillePotentiel gppTemp = new GrillePotentiel(gpTemp,motsPot);
		
//		System.out.println("006 " + m);
//		System.out.println("005 " + gpTemp.getPlaces().get(0).toString());
//		System.out.println("000 " + gppTemp.getMotsPot().get(0).toString());
//		System.out.println("001 " + gppTemp.getMotsPot().size());
//		System.out.println("002 " + gpTemp.getPlaces().size());
		
		//System.out.println("gptemp  emplacement : " + gpTemp.getPlaces().size());
		//System.out.println("gpptemp dic : " + gppTemp.getMotsPot().size());
		return gppTemp;
	}
	
	// Getter
	public List<Dictionnaire> getMotsPot(){
		return motsPot;
	}
	
	public GrillePotentiel copy() {
		GrillePotentiel copie = new GrillePotentiel(this.gp, this.d);
		return copie;
	}
	
	
	public GrillePlaces getGrillesMots() {
		return gp;
	}
	// BONUS
	
	public GrillePotentiel(GrillePlaces grille, List<Dictionnaire> listDico){
		this.gp = grille;
		
		
		
		
		// Filtrer
		int cptLD = 0;
		for(Emplacement i:this.gp.getPlaces()) {
			//if(!i.estPlein()) {
			
				Dictionnaire tempD = listDico.get(cptLD).copy();
				cptLD ++;
				
				tempD.filtreLongueur(i.size());
				
				
	
				
				int cpt = 0;
				
				
				for(Case c:i.getLettre()) {
					char chr = c.getChar();
					if(chr == ' ') {
						//System.out.println(cpt + "blank");
					}
					else {
						//System.out.print(cpt);
						//System.out.println(c.getChar());
						tempD.filtreParLettre(chr, cpt);
					}
					cpt ++;
				}
				//System.out.println(tempD.size());
				motsPot.add(tempD);
			//}
		}
		
		//System.out.println(motsPot.size());
		//System.out.println(grille.toString());
		
		// Detection contraintes
		
		for(Emplacement i:grille.getHorizontaux()) {
			for(Emplacement j:grille.getVerticaux()) {
				for(Case c1:i.getLettre()) {
					for(Case c2:j.getLettre()) {
						if(c1.equals(c2) && c1.isVide()) {
							int im1,im2,ic1,ic2;
							
							//System.out.println("c1 "+ c1.getCol()+ "_" + c1.getLig());
							//System.out.println("c2 "+c2.getCol() + "_"+ c2.getLig());
							

							
							im1 = grille.getPlaces().indexOf(i);
							im2 = grille.getPlaces().indexOf(j);
							
							ic1 = i.getLettre().indexOf(c1);
							ic2 = j.getLettre().indexOf(c2);
							
							
							//System.out.println(" " + im1 + " " + im2 + " " + ic1+ " " + ic2);
							IContrainte contrainteToAdd = new CroixContrainte(im1,ic1,im2,ic2);
							contraintes.add(contrainteToAdd); 
							//System.out.println(contraintes.size());
						}
					}
				}
			}
		}
		
		
		if(this.propage()) {
			System.out.println("Fini");
		}
		else {
			System.out.println("Irr�alisable");
		}
	}
}