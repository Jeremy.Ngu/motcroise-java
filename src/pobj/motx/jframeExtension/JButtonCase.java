package pobj.motx.jframeExtension;

import javax.swing.JButton;

public class JButtonCase extends JButton{
	private int row;
	private int col;
	
	public JButtonCase(int row, int col) {
		super();
		this.row = row;
		this.col = col;
	}
	
	public int getRow() {
		return this.row;
	}
	
	public int getCol() {
		return this.col;
	}
}
