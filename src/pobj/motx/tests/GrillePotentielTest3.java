package pobj.motx.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import pobj.motx.grille.Grille;
import pobj.motx.grille.GrilleLoader;
import pobj.motx.grille.GrillePlaces;
import pobj.motx.grille.GrillePotentiel;
import pobj.motx.structure.Dictionnaire;

public class GrillePotentielTest3 {

	@Test
	public void testMakeEasy2() {

		Dictionnaire gut = Dictionnaire.loadDictionnaire("data/frgut.txt");
		// grille 6x5, mots sans croisement
		Grille gr = GrilleLoader.loadGrille("data/easy.grl");

		assertEquals(5, gr.nbCol());
		assertEquals(5, gr.nbLig());

		// System.out.println(gr);

		GrillePlaces grille = new GrillePlaces(gr);

		GrillePotentiel gp = new GrillePotentiel(grille, gut);

		assertTrue(gp.getMotsPot().get(2).size() > 1);

		GrillePotentiel gp2 = gp.fixer(2, "chats");

		assertTrue(gp.getMotsPot().get(2).size() > 1);

		int[] expected = { 245, 302, 1 };
		GrillePotentielTest.testNombrePot(gp2, expected);

		// System.out.println(gp2.getGrillePlaces().getGrille());

		System.out.println("Succès test GrillePotentiel : make easy 2.");
	}

}
