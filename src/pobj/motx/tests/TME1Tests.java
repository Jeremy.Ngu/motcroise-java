package pobj.motx.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CaseTest.class, GrilleTest.class, GrillePlacesTest.class })
public class TME1Tests {

}
