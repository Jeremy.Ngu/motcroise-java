package pobj.motx.tests;

import static org.junit.Assert.*;
import static pobj.motx.tests.GrillePotentielTest.testNombrePot;

import org.junit.Test;

import pobj.motx.grille.Grille;
import pobj.motx.grille.GrilleLoader;
import pobj.motx.grille.GrillePlaces;
import pobj.motx.grille.GrillePotentiel;
import pobj.motx.structure.Dictionnaire;

public class GrillePotentielTest5 {

	@Test
	public void testMedium() {

		Dictionnaire gut = Dictionnaire.loadDictionnaire("data/frgut.txt");
		Grille gr = GrilleLoader.loadGrille("data/medium.grl");

		assertEquals(5, gr.nbCol());
		assertEquals(5, gr.nbLig());

		// System.out.println(gr);

		GrillePlaces grille = new GrillePlaces(gr);

		GrillePotentiel gp = new GrillePotentiel(grille, gut);

		assertTrue(!gp.isDead());

		assertEquals(9, gp.getContraintes().size());

		int[] expected = { 5916, 5916, 5320, 5916, 5916, 5320 };

		testNombrePot(gp, expected);

		System.out.println("Succès test GrillePotentiel : medium.");

	}

	@Test
	public void testEasy() {

		Dictionnaire gut = Dictionnaire.loadDictionnaire("data/frgut.txt");
		Grille gr = GrilleLoader.loadGrille("data/easy.grl");

		assertEquals(5, gr.nbCol());
		assertEquals(5, gr.nbLig());

		// System.out.println(gr);

		GrillePlaces grille = new GrillePlaces(gr);

		GrillePotentiel gp = new GrillePotentiel(grille, gut);

		assertTrue(!gp.isDead());

		assertEquals(2, gp.getContraintes().size());

		int[] expected = { 5916, 5688 , 5916};

		testNombrePot(gp, expected);

		System.out.println("Succès test GrillePotentiel : easy.");

	}
	
	@Test
	public void testHard() {

		Dictionnaire gut = Dictionnaire.loadDictionnaire("data/frgut.txt");
		Grille gr = GrilleLoader.loadGrille("data/hard.grl");

		assertEquals(5, gr.nbCol());
		assertEquals(5, gr.nbLig());

		// System.out.println(gr);

		GrillePlaces grille = new GrillePlaces(gr);

		GrillePotentiel gp = new GrillePotentiel(grille, gut);

		assertTrue(!gp.isDead());

		assertEquals(25, gp.getContraintes().size());

		int[] expected = { 5916, 5916, 5916, 5916, 5185, 5916, 5916, 5916, 5916, 5185 };

		testNombrePot(gp, expected);

		System.out.println("Succès test GrillePotentiel : hard.");
	}

}
