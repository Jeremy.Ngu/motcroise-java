package pobj.motx.strategies;

import java.util.List;

import pobj.motx.interfaces.ICSP;
import pobj.motx.interfaces.IChoixVar;
import pobj.motx.interfaces.IVariable;

public class StratMin implements IChoixVar{

	@Override
	public IVariable chooseVar(ICSP Problem) {
		// TODO Auto-generated method stub
	    List<IVariable> dico = Problem.getVars();
	    IVariable min        = dico.get(0);
	    for(IVariable var:dico){
		if(var.getDomain().size() < min.getDomain().size())
		    min = var;
	    }
	    return min;
	}

}
