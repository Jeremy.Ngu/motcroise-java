package pobj.motx.filtre;

import java.util.ArrayList;
import java.util.List;

import pobj.motx.grille.GrillePotentiel;
import pobj.motx.interfaces.ICSP;
import pobj.motx.interfaces.IVariable;
import pobj.motx.structure.DicoVariable;
import pobj.motx.structure.Dictionnaire;


public class MotX implements ICSP{
	private GrillePotentiel gp;
	private List<DicoVariable> listDicoV= new ArrayList<DicoVariable>();
	public MotX(GrillePotentiel gp) {
		this.gp = gp;
		
		
		List<Dictionnaire> listD = gp.getMotsPot();
		//System.out.println(listD.size());
		for(int i=0; i< listD.size();i++) {
			if(!gp.getgrillePlaces().getPlaces().get(i).estPlein())
				listDicoV.add(new DicoVariable(i,gp));
		}
		
	}

	@Override
	public List<IVariable> getVars() {
		@SuppressWarnings("unchecked")
		List<IVariable> retour = (List<IVariable>)(List<?>) listDicoV;
		return retour;
	}

	@Override
	public boolean isConsistent() {
		for(DicoVariable dico:listDicoV) {
			if (dico.getDomain().size() == 0)
				return false;
		}
		return true;
	}

	@Override
	public ICSP assign(IVariable vi, String val) {
		// TODO Auto-generated method stub
		if(vi instanceof DicoVariable) {
			DicoVariable dv = (DicoVariable)vi;
			GrillePotentiel gpTemp = gp.fixer(dv.getIndice(),val);
			//System.out.println(gpTemp.toString());
			return new MotX(gpTemp);
		}
		
		return null;
	}
	
	public GrillePotentiel getGP() {
		return this.gp;
	}
	
}