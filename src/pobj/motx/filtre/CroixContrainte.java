package pobj.motx.filtre;

import pobj.motx.grille.GrillePotentiel;
import pobj.motx.interfaces.IContrainte;
import pobj.motx.structure.Dictionnaire;
import pobj.motx.structure.EnsembleLettre;

public class CroixContrainte implements IContrainte {
	int c1,c2;
	int m1,m2;
	
	public CroixContrainte(int m1,int c1, int m2,int c2) {
		this.c1 = c1;
		this.c2 = c2;
		this.m1 = m1;
		this.m2 = m2;	
	}
	
	
	public int getEmplacement1() {
		return m1;
	}
	public int getCase1() {
		return c1;
	}
	
	public int getCase2() {
		return c2;
	}
	
	public int getEmplacement2() {
		return m2;
	}
	
	@Override
	public int reduce(GrillePotentiel grille) {
		
		
		
		Dictionnaire dico1 = grille.getMotsPot().get(m1);
		Dictionnaire dico2 = grille.getMotsPot().get(m2);
		
		//System.out.println("m1 : " + m1 + "// c1 : " + c1 + "// m2 : " + m2 + "// c2 : " + c2);
		
		
		EnsembleLettre el1 = dico1.getEnsembleLettre(c1);
		EnsembleLettre el2 = dico2.getEnsembleLettre(c2);
		
//		EnsembleLettre el1 = dico1.charAt(c1);
//		EnsembleLettre el2 = dico2.charAt(c2);
		
		int motsSupp1 = 0;
		int motsSupp2 = 0;
		
		if(el1 == null || el2 == null) {
			return 0;
		}
		el1.retainAll(el2);
		el2.retainAll(el1);
		
		motsSupp1 = dico1.retainAll(el1, c1);
		motsSupp2 = dico2.retainAll(el2, c2);
		
		return motsSupp1 + motsSupp2;
	}
	
	public boolean equals(Object o) {
		CroixContrainte cc = (CroixContrainte)o;
		if(c1 != cc.getCase1()) {
			return false;
		}
		
		if(c2 != cc.getCase2()) {
			return false;
		}
		
		if(m1 != cc.getEmplacement1()) {
			return false;
		}
		if(m2 != cc.getEmplacement2()) {
			return false;
		}
		return true;
	}
	
	public String toString() {
		return " m1 : " + m1 + " c1 : " + c1 + " m2 : " + m2  +" c2 : " + c2;
	}

}
