package pobj.motx.ihm;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

import pobj.motx.filtre.CSPSolver;
import pobj.motx.filtre.MotX;
import pobj.motx.grille.Grille;
import pobj.motx.grille.GrilleLoader;
import pobj.motx.grille.GrillePlaces;
import pobj.motx.grille.GrillePotentiel;
import pobj.motx.interfaces.ICSP;
import pobj.motx.jframeExtension.JButtonCase;
import pobj.motx.structure.Dictionnaire;

public class Window extends JFrame{
	
	public static final long serialVersionUID = -4939540011287453046L;
	
	// FileChoosers
	private JFileChooser fChooser = new JFileChooser(System.getProperty("user.dir"));
	private JFileChooser fSave = new JFileChooser(System.getProperty("user.dir"));
	
	// Holds cases and changes
	private Grille gr = null;
	
	// Stores the original grid
	private Grille grOriginal = null;
	
	private Dictionnaire dico = Dictionnaire.loadDictionnaire("data/frgut.txt");
	// Use to solve

	
	// "create" mode is used to allow squarres to hold a char or not
	// "edit" mode is used to put char in available squarres
	private String mode = "create";
	
	
	// Window creation
	public Window() {
		// Create Window
		super("MotCroise Window");
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setSize(800,600);
		// this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.setLocationRelativeTo(null);
				
		// Set File Chooser
		
		
		fSave.setDialogTitle("Save a File");
		fSave.setFileFilter(new FileNameExtensionFilter("Grille files", "grl"));
		
	
		JPanel contentPane = (JPanel) this.getContentPane();
		// contentPane.setLayout( new GridLayout(3,1, 20, 20));
		
		
		// JTextField row = new JTextField("Number of row (Max 32)");	
		// JTextField col = new JTextField("Number of column (Max 32)");
		
		JMenuBar mnuBar = createMenuBar();
		contentPane.add(mnuBar, BorderLayout.NORTH);
        
		JPanel board = new JPanel(new GridLayout(1,1));
		contentPane.add(board);        
	}
	
	
	// Creation of a menu bar 
	private JMenuBar createMenuBar() {
			JMenuBar mnuBar = new JMenuBar();
			
			// File edition, create + save
			JMenu mnuFile = new JMenu("File");
			
			JMenuItem mnuFileItem1 = new JMenuItem("New Grid ...");
			mnuFileItem1.addActionListener(this::mnuNewGridListener);
			mnuFileItem1.setIcon( new ImageIcon("src/pobj/motx/ihm/images/grid.jpg") );
			
			
			JMenuItem mnuFileItem2 = new JMenuItem("Open File ...");
			mnuFileItem2.setIcon( new ImageIcon("src/pobj/motx/ihm/images/file.png") );
			mnuFileItem2.addActionListener( (e) -> mnuOpenFileListener(e));
			
			JMenuItem mnuFileItem3 = new JMenuItem("Save ...");
			mnuFileItem3.addActionListener( (e) -> mnuSaveFileListener(e) );
			mnuFileItem3.setIcon( new ImageIcon("src/pobj/motx/ihm/images/save.jpg") );
			
			JMenuItem mnuFileItem4 = new JMenuItem("Load Dictionnary...");
			mnuFileItem4.setIcon( new ImageIcon("src/pobj/motx/ihm/images/dico.png") );
			mnuFileItem4.addActionListener( (e) -> mnuOpenDicoListener(e));
			
			mnuFile.add(mnuFileItem1);
			mnuFile.add(mnuFileItem2);
			mnuFile.add(mnuFileItem3);
			mnuFile.add(mnuFileItem4);
			
			mnuBar.add(mnuFile);
			
			
			
			JButton btnClear = new JButton("Clear");
			JButton btnSolve = new JButton("Solve");
			JButton btnModeCreation = new JButton("Mode Create");
			JButton btnModeEdit = new JButton("Mode Edit");
			btnClear.addActionListener( (e) -> mnuClearGridListener(e) );
			btnSolve.addActionListener( (e) -> mnuSolveGridListener(e) );
			btnModeCreation.addActionListener(e -> this.mode = "create");
			btnModeEdit.addActionListener(e -> this.mode = "edit");
			
			mnuBar.add(btnClear);
			mnuBar.add(btnSolve);
			mnuBar.add(btnModeCreation);
			mnuBar.add(btnModeEdit);
			
			return mnuBar;
	}
	
	
	// Use to rerender the board after a critical update of the whole board
	private void updateBoard(Grille grille) {
		int row = gr.nbLig();
		int col = gr.nbCol();
		
		// Remove old board
		this.getContentPane().remove(1);

		
		// Create a whole new board
		JPanel board = new JPanel(new GridLayout(row,col));
		JPanel[][] squarres= new JPanel[row][col];
		
		for (int i= 0; i < row; i++) {
            for (int j= 0; j < col; j++) {
                squarres[i][j] = new JPanel();
                
                char c = gr.getCase(i, j).getChar();
                
            	String letter = Character.toString(c);
            	JButtonCase btnCase = new JButtonCase(i,j);
            	btnCase.setText(letter);
            	btnCase.addActionListener((e) -> caseClickListener(e));
            	squarres[i][j].add(btnCase);
            
                board.add(squarres[i][j]);
            }
        }
		
		this.getContentPane().add(board);
		this.getContentPane().revalidate();
		this.getContentPane().repaint();
	}
	
	private void updateBoard() {
		int row = gr.nbLig();
		int col = gr.nbCol();
		
		// Remove old board
		this.getContentPane().remove(1);

		
		// Create a whole new board
		JPanel board = new JPanel(new GridLayout(row,col));
		JPanel[][] squarres= new JPanel[row][col];
		
		for (int i= 0; i < row; i++) {
            for (int j= 0; j < col; j++) {
                squarres[i][j] = new JPanel();
                
                char c = gr.getCase(i, j).getChar();
                
            	String letter = Character.toString(c);
            	JButtonCase btnCase = new JButtonCase(i,j);
            	btnCase.setText(letter);
            	btnCase.addActionListener((e) -> caseClickListener(e));
            	squarres[i][j].add(btnCase);
            
                board.add(squarres[i][j]);
            }
        }
		
		this.getContentPane().add(board);
		this.getContentPane().revalidate();
		this.getContentPane().repaint();
	}
	

	public static boolean isNumeric(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch(NumberFormatException e) {
			return false;
		}
	}
	
	
	// Edition & Creation of a board by clicking on buttons
	private void caseClickListener(ActionEvent event) {	
		JButtonCase btnCase = (JButtonCase)event.getSource();
		// Change the grid
		if(this.mode == "create") {
			// Change noChar to Char
			if(btnCase.getText().equals("*")) {
				btnCase.setText(" ");
				gr.getCase(btnCase.getRow(), btnCase.getCol()).setChar(' ');
			}
			// Change Char to noChar
			else {			
				btnCase.setText("*");
				gr.getCase(btnCase.getRow(), btnCase.getCol()).setChar('*');
			}
		}else {
			if(!btnCase.getText().equals("*")){
				JFrame jform = new JFrame();
				jform.setVisible(true);
				jform.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				jform.setSize(200,150);
				// this.setExtendedState(JFrame.MAXIMIZED_BOTH);
				jform.setLocationRelativeTo(null);
				
				JTextField textChar = new JTextField();
				JButton btnValidate = new JButton("Validate");
				
				// Changes the value in the button and grid
				btnValidate.addActionListener( (e) -> 
					{
						if(textChar.getText().length() == 1) {
							char c = textChar.getText().charAt(0);
							if(Character.isLetter(c) || c == ' ' || c == '*'){
								btnCase.setText(textChar.getText());
								gr.getCase(btnCase.getRow(), btnCase.getCol()).setChar(c);
								jform.dispose();
							}
						}
						else {
							JOptionPane.showMessageDialog(this, "Please enter a character");
						}
					}
				);
				
				JPanel jformPane = (JPanel) jform.getContentPane();
				jformPane.setLayout(null);
				
				textChar.setHorizontalAlignment(SwingConstants.CENTER);
				textChar.setBounds(75, 30, 50, 30);
				btnValidate.setBounds(25, 70, 150, 30);
				
				jformPane.add(textChar);
				jformPane.add(btnValidate);
			}
		}
		
	}
	
	// Creation of a new grid
	private void mnuNewGridListener( ActionEvent event ) {
		JFrame jform = new JFrame();
		jform.setVisible(true);
		jform.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		jform.setSize(400,150);
		// this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		jform.setLocationRelativeTo(null);
		
		JTextField textRow = new JTextField("Number of rows");
		JTextField textCol = new JTextField("Number of columns");
		JButton btnValidate = new JButton("Validate");
		btnValidate.addActionListener( (e) -> 
		{
			if(isNumeric(textRow.getText()) && isNumeric(textCol.getText())) {
				
				
				int rows = Integer.parseInt(textRow.getText());
				int cols = Integer.parseInt(textCol.getText());
				
				
				this.grOriginal = new Grille(rows,cols);
				this.gr = grOriginal.copy();
				this.mode = "create";
				
				jform.dispose();
				updateBoard();
				
			}
			else {
				JOptionPane.showMessageDialog(this, "Please use integer values");
			}
		}
				);
		
		JPanel jformPane = (JPanel) jform.getContentPane();
		jformPane.setLayout(null);
		
		textRow.setHorizontalAlignment(SwingConstants.CENTER);
		textCol.setHorizontalAlignment(SwingConstants.CENTER);
		textRow.setBounds(50, 10, 125, 20);
		textCol.setBounds(200, 10, 125, 20);
		btnValidate.setBounds(140, 50, 100, 25);
		
		jformPane.add(textRow);
		jformPane.add(textCol);
		jformPane.add(btnValidate);
		
		

	}
	
	// Reset the grid as it was
	private void mnuClearGridListener( ActionEvent event ) {
		if(grOriginal != null) {
			gr = grOriginal.copy();
			updateBoard();
		}
	}
	
	private void mnuSolveGridListener( ActionEvent event ) {
		
		GrillePlaces grille = new GrillePlaces(gr);
		GrillePotentiel gp = new GrillePotentiel(grille, this.dico);
		
		// System.out.println(gp);
		// assertTrue(! gp.isDead());

		ICSP problem = new MotX(gp);
		CSPSolver solver = new CSPSolver();

		// solver.setStrat(new StratFirst());
		// solver.setStrat(new StratMin());
		
		long timestamp = System.currentTimeMillis();
		ICSP solution = solver.solve(problem);

		System.out.println(((MotX)solution).getGP().getGrillesMots().toString());
		
		// Grille newGrille = ((MotX)solution).getGP().getGrillesMots().getGrille().copy();
		
		this.gr = ((MotX)solution).getGP().getGrillesMots().getGrille().copy();
		updateBoard();
	}
	
	
	// Opening new file and loading into the board
	private void mnuOpenFileListener( ActionEvent event ) {
		this.fChooser.setFileFilter(new FileNameExtensionFilter("Files ending in .grl", "grl"));
		int result = fChooser.showOpenDialog(this);
		
		if(result == JFileChooser.APPROVE_OPTION) {
			// user selected a file
			File selectedFile = fChooser.getSelectedFile();
			gr = GrilleLoader.loadGrille(selectedFile.getPath());
			grOriginal = gr.copy();
			updateBoard();
		}
	}
	
	// Loads a new dictionnary
	private void mnuOpenDicoListener( ActionEvent event ) {
		this.fChooser.resetChoosableFileFilters();
		int result = fChooser.showOpenDialog(this);
		
		
		if(result == JFileChooser.APPROVE_OPTION) {
			// user selected a file
			File selectedFile = fChooser.getSelectedFile();
			this.dico = Dictionnaire.loadDictionnaire(selectedFile.getPath());
		}
	}
	
	// Save a grid into a file
	private void mnuSaveFileListener( ActionEvent event ) {
		int result = fSave.showSaveDialog(this);
		if(result == JFileChooser.APPROVE_OPTION) {
			// user selected a file
			File selectedFile = fSave.getSelectedFile();
			try {
				FileWriter fw = new FileWriter(selectedFile.getPath()) ;
				fw.write(gr.nbLig() + " " + gr.nbCol());
				fw.write("\n");
				for(int i = 0; i < gr.nbLig(); i++) {
					for(int j = 0 ; j < gr.nbCol(); j++) {
						fw.write(gr.getCase(i, j).getChar());
					}
					fw.write("\n");
				}
				fw.flush();
				fw.close();
			}
			catch(Exception e) {
				JOptionPane.showMessageDialog(this, "Couldn't write down");
			}
		}
		else {
			JOptionPane.showMessageDialog(this, "No Valid File Name");
		}
	}
	
	
	public String getGrilleToText() {
		return null;
	}
	
	public static void main(String[] args) {
		Window myW = new Window();
		myW.setVisible(true);
	}


	

}
