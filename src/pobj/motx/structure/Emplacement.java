package pobj.motx.structure;

import java.util.*;

public class Emplacement{
	private List<Case> lettre;

	public Emplacement(){
		lettre = new ArrayList<Case>();
	}

	public void add(Case c){
		lettre.add(c);
	}
	
	public List<Case> getLettre() {
		return lettre;
	}
	
	public int size(){
		return lettre.size();
	}

	public String toString(){
		String output = "";
		for(Case ca:lettre){
			output = output + ca.getChar();
		}
		return output;
	}
	
	public boolean estPlein() {
		for(Case ca:lettre){
			if(ca.getChar() == ' ') {
				return false;
			}
		}
		return true;
	}

	
}