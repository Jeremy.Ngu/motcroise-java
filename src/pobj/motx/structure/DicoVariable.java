package pobj.motx.structure;

import java.util.List;

import pobj.motx.grille.GrillePotentiel;
import pobj.motx.interfaces.IVariable;



public class DicoVariable implements IVariable {
	private int indice;
	private GrillePotentiel gp;
	private List<String> domain;
	
	public DicoVariable(int indice, GrillePotentiel gp) {
		this.indice = indice;
		this.gp = gp;
		this.domain = gp.getMotsPot().get(indice).getMots();
	}
	
	@Override
	public List<String> getDomain() {
		domain = gp.getMotsPot().get(indice).getMots();
		return domain;
	}
	
	public String toString() {
		return "DicoVariable" + " : Indice : " + indice + " // List<String>.size ; " + domain.size();
	}
	
	public int getIndice() {
		return indice;
	}
	
	
}