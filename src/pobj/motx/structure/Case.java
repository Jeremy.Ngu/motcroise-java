package pobj.motx.structure;

public class Case{

	private int lig;
	private int col;
	private char defChar;
	
	public Case(int lig, int col, char c){
		this.lig = lig;
		this.col = col;
		this.defChar = c;

	}



	// OPERATEUR
	public boolean isVide(){
		if(defChar == ' '){
			return true;
		}
		else{
			return false;
		}
	}
	public boolean isPleine(){
		if(defChar == '*'){
			return true;
		}
		else{
			return false;
		}
	}

	// ACCESSEUR
	public int getLig(){
		return lig;
	}
	public int getCol(){
		return col;
	}
	public char getChar(){
		return defChar;
	}

	public void setChar(char c){
		this.defChar = c;
	}
	
	public String toString() {
		return "Case : " + lig + " / " + col;
	}

}