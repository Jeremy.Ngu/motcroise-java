package pobj.motx.structure;


import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Un ensemble de mots.
 *
 */
public class Dictionnaire {

	// stockage des mots
	private List<String> mots = new ArrayList<>();
	private EnsembleLettre[] cache = null;
	
	/**
	 * Ajoute un mot au Dictionnaire, en dernière position.
	 * @param mot à ajouter, il sera stocké en minuscules (lowerCase)
	 */
	public void add(String mot) {
		mots.add(mot.toLowerCase());
	}

	/**
	 * Taille du dictionnaire, c'est à dire nombre de mots qu'il contient.
	 * @return la taille
	 */
	public int size() {
		return mots.size();
	}
	
	/**
	 * Accès au i-eme mot du dictionnaire.
	 * @param i l'index du mot recherché, compris entre 0 et size-1.
	 * @return le mot à cet index
	 */
	public String get(int i) {
		return mots.get(i);
	}

	/**
	 * Rend une copie de ce Dictionnaire.
	 * @return une copie identique de ce Dictionnaire
	 */
	public Dictionnaire copy () {
		Dictionnaire copy = new Dictionnaire();
		copy.setCache(cache);
		copy.mots.addAll(mots);
		return copy;
	}
	
	public void setCache(EnsembleLettre[] cache) {
		this.cache = cache;
	}
	public EnsembleLettre[] getCache() {
		return cache;
	}

	/**
	 * Retire les mots qui ne font pas exactement "len" caractères de long.
	 * Attention cette opération modifie le Dictionnaire, utiliser copy() avant de filtrer pour ne pas perdre d'information.
	 * @param len la longueur voulue 
	 * @return le nombre de mots supprimés
	 */
	public int filtreLongueur(int len) {
		List<String> cible = new ArrayList<>();
		int cpt=0;
		for (String mot : mots) {
			if (mot.length() == len)
				cible.add(mot);
			else
				cpt++;
		}
		mots = cible;
		
		if(cpt == 0) {
			cache = null;
		}
		return cpt;
	}
	
	public int filtreParLettre(char c, int i) {
		List<String> cible = new ArrayList<>();
		int cpt=0;
		for (String mot : mots) {
			if (mot.charAt(i) == c) {
				cible.add(mot);
			}
			else {
				cpt++;
			}
		}
		mots = cible;
		if(cpt == 0) {
			if(cache != null)
				cache[i] = null;
		}
		return cpt;
	}

	public static Dictionnaire loadDictionnaire(String path) {
		Dictionnaire dico = new Dictionnaire();
		try (BufferedReader br = new BufferedReader(new FileReader(path))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		       dico.add(line);
		    }
		}
		catch(Exception e){
			System.out.println("Erreur lors du chargement");
		}
		return dico;
	}
	
	public EnsembleLettre getEnsembleLettre(int indice) {
		EnsembleLettre el = new EnsembleLettre();
		//System.out.println("\n" + indice);
		//System.out.println("Nouveau Ensemble :" + indice);;
		for(String i:mots) {
			el.add(i.charAt(indice));
		}
		return el;
	}
	
	public int retainAll(EnsembleLettre el,int i) {
		List<String> cible = new ArrayList<>();
		int cpt=0;
		for (String mot : mots) {
			if (el.contain(mot.charAt(i))) {
				cible.add(mot);
			}
			else {
				cpt++;
			}
		}
		mots = cible;
		return cpt;
	}
	
	public List<String> getMots(){
		return mots;
	}
	
	@Override
	public String toString() {
		if (size() == 1) {
			return mots.get(0);
		} else {
			return "Dico size =" + size();
		}
	}
	
	public EnsembleLettre charAt(int index) {
		int taille = 0;
		if(mots.size() == 0) {
			return null;
		}
		
		if(cache == null) {
			taille = (mots.get(0)).length();
			cache = new EnsembleLettre[taille];
		}
		
		if(cache[index] == null) {
			cache[index] = getEnsembleLettre(index);
			//System.out.println(cache[index].toString());
		}
		
		return cache[index];
	}
	
}
