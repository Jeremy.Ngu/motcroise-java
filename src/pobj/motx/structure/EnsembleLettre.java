package pobj.motx.structure;

import java.util.*;

public class EnsembleLettre {
	private List<Character> lc = new ArrayList<Character>();
	
	
	public EnsembleLettre() {}
	
	public List<Character> getListCharacter() {
		return lc;
	}
	
	public void add(char c){
		if(lc.contains(c)) {
			
		}
		else {
			lc.add(c);
		}
	}
	
	public boolean contain(char c) {
		return lc.contains(c);
	}
	
	public void retainAll(EnsembleLettre el) {
		lc.retainAll(el.getlc());
	}
	
	public List<Character> getlc(){
		return lc;
	}
	
	public String toString() {
		String s = "";
		for(char c:lc) {
			s += c + " - ";
		}
		return s;
	}
}
