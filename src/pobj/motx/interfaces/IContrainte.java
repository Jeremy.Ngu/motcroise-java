package pobj.motx.interfaces;

import pobj.motx.grille.GrillePotentiel;

public interface IContrainte {
	
	int reduce(GrillePotentiel grille);
	
}
