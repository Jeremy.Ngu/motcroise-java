package pobj.motx.interfaces;

public interface IChoixVar {
	public abstract IVariable chooseVar(ICSP Problem);
}
