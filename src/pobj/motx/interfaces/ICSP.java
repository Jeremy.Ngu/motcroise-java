package pobj.motx.interfaces;

import java.util.List;

public interface ICSP{
	
	public abstract List<IVariable> getVars();
	public abstract boolean isConsistent();
	public abstract ICSP assign(IVariable vi,String val);
	
}
